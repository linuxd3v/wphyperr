#!/usr/bin/env bash

#Safety 1: Everything relevant to this directory
thisDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $thisDir

#Safety 2: Dont run as root
if [ $(id -u) = 0 ]; then
   echo "Do not run as root!"
   exit 1
fi

#Checking for config file and sourcing it.
if [ ! -f ".env" ]; then
    echo "Must execute from ENV directory"
    exit 1
fi
source .env


echo "Setting up dir structure for: '$ENV_NAME' environment"

echo "Creating docker persistent volumes directories"
mkdir -p $FS_DIR
mkdir -p $FS_DIR/mysql/{data,config}
mkdir -p $FS_DIR/redis/$REDIS_PORT
mkdir -p $FS_DIR/application

touch $FS_DIR/application/index.php
printf "<?php echo \"you made it\";?>" >> $FS_DIR/application/index.php
sudo chown -R www-data:www-data $FS_DIR/application

mkdir -p $CERTS_DIR


echo OK