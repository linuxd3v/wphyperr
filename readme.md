# WPHYPERR

Worpdress stack boilerplate project for personal use and demo purposes mostly  - optimized for  quick response times.  
  
There are many of such projects exist, I do however like to customize things sometimes so  would rather not piggy back on other projects.

## Suggested hosting specs
SSD (better) or NVME(best) VPS with at least 3Gb ram and 3 vcpu, 20GB+ drive space space.  
This stack will probably work on significantly less resources, but why bother when servers are so cheap these days and above specs can be had for 5$ a month or less.  
A little pro tip - if you are on the budget head over to https://lowendbox.com/ - great resource that always features inexpensive or heavily discounted VPS deals. 

## Prerequisites (heavily simplified)
- whatever runs docker box (but just definately linux)
- root account
- docker installed and running
- user added to docker group


## Features
- nginx (default)
- or apache+mod_event(modules stripped to minimum needed)
- php-fpm (fastest way to run php)
- server signatures stripped (although this is unlikely to trick anyone - wordpress is wordpess)
- varnish cache
- haproxy for TLS termination and http2
- only TLSv1.2 or higher
- http2
- static assets routed to pre-compressed brotli/gzip if available
- dynamic brotli compression with gzip fallback on text-based content
- redis container (used for PHP sessions by default, but can be used for other things too - like caching)
- all logging routed to STDOUT
- msmtp for smtp (meaning emails actually work in wordpress)


## Questions and answers
1) **Why develoment and production?**  
It's fairly obvious that everyone has different needs. However for my personal develoment I tend to do local development 100% on selfhosted servers that are inside of my network never exposing them to the internet.
It is not without it's own challenges - as many things may work differently, such as SSL termination and I need to account for that. Also development environment may have display errors enabled and other things that may help in development process.  

2) to be continued
  


## How to use  
1. Create a bridge network so all containers can talk to each other, ex:

```bash
docker network create --attachable -d bridge infranet
```

2. Clone this repo on your VPS/SERVER.
Go into your environment (env-development, env-production), rename .env.template to .env and change configuration in .env file
to fit your needs.

3. You can go over configs ad asjust, however defaults are already sane.

4. Setup directories.
There is included script: `prep-fs-infra.sh` file that helps with that, however you can also do this manually.
The architechtural goal here is this:  
- `$FS_DIR` - directory that stores all the content for all the containers you may run.  This makes backups trivial, simple rsync can backup your entire mysql, redis, wordpress content.
- `$CERTS_DIR` - ssl certificates.
Most modern browsers nowadays don't allow long term certificates (max you can do is one year) with that automation of SSL certificate generations is much preferred.  
Read here if you want to setup SSL distribution pipeline using wonderful let'sencrypt service:     
https://bytepursuits.com/letsencrypt-ssl-certificates-generation-using-dnsrobocert
- `$CODE_DIR` - your wordpress location. Wordpress is unfortunately not known for cleanest of architecture. Here I would typically only expose public directory to webserver, but with wordpress it is not possible. (well - not without bedrock project, but such setup would then have its own issues).


5. Download Wordpress and unzip it into application directory:
```bash
cd "whatever your code directory is"
wget https://wordpress.org/wordpress-5.6.2.zip .
unzip https://wordpress.org/wordpress-5.6.2.zip
mv wordpress/* .
rm -rf wordpress
rm -rf https://wordpress.org/wordpress-5.6.2.zip
sudo chown -R www-data:www-data .

find . -type d -exec chmod 755 {} \;  # Change directory permissions rwxr-xr-x
find . -type f -exec chmod 644 {} \;  # Change file permissions rw-r--r--
```

6. Edit wp-config.php

```php

//CUSTOM CHANGES START
//==========================================================================
define( 'WP_HOME', 'https://your-domain.tld:31753' );
define( 'WP_SITEURL', 'https://your-domain.tld:31753' );

// Override $_SERVER['REMOTE_ADDR'] on Wordpress.
// WP is badly architected and won't allow us to set this value properly anywhere.
// Various WP code area use differnt headers  inconsistently for ip detection.
if (isset($_SERVER["HTTP_X_CLIENT_IP"])) {
    $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_X_CLIENT_IP"];
}


// Allow WordPress to detect HTTPS when used behind a reverse proxy or a load balancer
// See https://codex.wordpress.org/Function_Reference/is_ssl#Notes
// If this is not set -> wordpress will enter into endless http -> https redirect loop.
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
    $_SERVER['HTTPS'] = 'on';
}
//==========================================================================
//CUSTOM CHANGES END
```


7. Launch the stack.
Note: you must do this from env directory:

```bash
cd development
docker-compose up -d
```


  
   

## License
MIT 