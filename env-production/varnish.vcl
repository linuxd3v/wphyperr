vcl 4.0;


# After VCL loaded, before any requests pass through it.
# Could be used to init VMODs.
sub vcl_init {

}


# Allowed ips/hosts to run purges
acl purge {
  "localhost";
  "127.0.0.1";
  "::1";
  "ENV_PROJECT_NAME-phpfpm";
}

# Called at the beginning of a request, after the complete request has been received and parsed.
# Its purpose is to decide whether or not to serve the request, how to do it, and, if applicable,
# which backend to use. also used to modify the request
sub vcl_recv {
    
 # Remove any Google Analytics based cookies
  set req.http.Cookie = regsuball(req.http.Cookie, "__utm.=[^;]+(; )?", "");
  set req.http.Cookie = regsuball(req.http.Cookie, "_ga=[^;]+(; )?", "");
  set req.http.Cookie = regsuball(req.http.Cookie, "_gat=[^;]+(; )?", "");
  set req.http.Cookie = regsuball(req.http.Cookie, "utmctr=[^;]+(; )?", "");
  set req.http.Cookie = regsuball(req.http.Cookie, "utmcmd.=[^;]+(; )?", "");
  set req.http.Cookie = regsuball(req.http.Cookie, "utmccn.=[^;]+(; )?", "");

  # Remove Optimizely Cookies
  set req.http.Cookie = regsuball(req.http.Cookie, "optim.=[^;]+(; )?", "");

  # Remove Gauges Cookies
  set req.http.Cookie = regsuball(req.http.Cookie, "_gau.=[^;]+(; )?", "");

  # Remove a ";" prefix in the cookie if present
  set req.http.Cookie = regsuball(req.http.Cookie, "^;\s*", "");

  # Allow purging
  if (req.method == "PURGE") {
    if (!client.ip ~ purge) { # purge is the ACL defined at the begining
      # Not from an allowed IP? Then die with an error.
      return (synth(405, "This IP is not allowed to send PURGE requests."));
    }
    # If you got this stage (and didn't error out above), purge the cached result
    return (purge);
  }

  if (req.method != "GET" &&
      req.method != "HEAD" &&
      req.method != "PUT" &&
      req.method != "POST" &&
      req.method != "TRACE" &&
      req.method != "OPTIONS" &&
      req.method != "DELETE") {
        # Non-RFC2616 or CONNECT which is unexpected.
        return (pipe);
  }

  if (req.method != "GET" && req.method != "HEAD") {
    # We only deal with GET and HEAD by default
    return (pass);
  }

  if(req.url ~ "^/cron.php") {
    return(pass);
  }
  if(req.url ~ "^/xmlrpc.php") {
    return(pass);
  }
  if (req.http.Authorization) {
    return(pass);
  }
  if(req.http.cookie ~ "(^|;\s*)(SESS=)") {
    return(pass);
  }

  if (req.url ~ "wp-admin|wp-login") {
      return (pass);
  }

  return (hash);
}


# The routine when we deliver the HTTP request to the user
# Last chance to modify headers that are sent to the client
sub vcl_deliver {
  if (obj.hits > 0) {
    set resp.http.X-vcache = "HIT";
  } else {
    set resp.http.X-vcache = "MISS";
  }
}



backend default {
    .host = "ENV_PROJECT_NAME-nginx";
    .port = "80";
}